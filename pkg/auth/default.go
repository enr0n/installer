// Copyright © 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"io/ioutil"

	"github.com/pkg/errors"
)

type DefaultLuks struct {
	options map[string]string
}

func NewDefaultLuks() (*DefaultLuks, error) {
	d := &DefaultLuks{
		options: make(map[string]string),
	}

	return d, nil
}

func (d *DefaultLuks) GetKey() ([]byte, error) {
	productUUID, err := ioutil.ReadFile(d.options["key"])
	if err != nil {
		return []byte{}, errors.Wrapf(err, "couldn't read key: %v", d.options["key"])
	}

	return productUUID, nil
}

func (d *DefaultLuks) SetOption(key string, value string) error {
	switch key {
	case "key":
	case "key_type":
	default:
		return errors.Errorf("invalid option passed to default auth: [%v]:[%v]", key, value)
	}

	d.options[key] = value

	return nil
}

func (d *DefaultLuks) GetOption(key string) string {
	return d.options[key]
}
