// Copyright © 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"io"
	"os"

	"github.com/pkg/errors"
	"golang.org/x/crypto/ssh/terminal"
)

type PasswordLuks struct {
	options map[string]string
}

func NewPasswordLuks() (*PasswordLuks, error) {
	p := PasswordLuks{}
	p.options = make(map[string]string)

	return &p, nil
}

func (p *PasswordLuks) GetKey() ([]byte, error) {
	// If we have a valid key and key_type, skip the interactive password
	// prompting
	if p.options["key"] != "" && p.options["key_type"] == "string" {
		return []byte(p.options["key"]), nil
	}

	if !terminal.IsTerminal(0) || !terminal.IsTerminal(1) {
		return []byte{}, errors.New("stdin/stdout should be terminal")
	}

	oldState, err := terminal.MakeRaw(0)
	if err != nil {
		return []byte{}, err
	}
	defer func() {
		_ = terminal.Restore(0, oldState)
	}()

	screen := struct {
		io.Reader
		io.Writer
	}{os.Stdin, os.Stdout}

	t := terminal.NewTerminal(screen, "")
	key, err := t.ReadPassword("Enter password: ")

	if err != nil {
		return []byte{}, errors.Wrapf(err, "failed to ReadPassword")
	}

	return []byte(key), nil
}

func (p *PasswordLuks) SetOption(key string, value string) error {
	switch key {
	case "key":
	case "key_type":
	default:
		return errors.Errorf("invalid option for password authentication: %v : %v", key, value)
	}

	p.options[key] = value

	return nil
}

func (p *PasswordLuks) GetOption(key string) string {
	return p.options[key]
}
