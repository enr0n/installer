// Copyright © 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"io/ioutil"

	"github.com/pkg/errors"
)

type USBLuks struct {
	options map[string]string
}

func NewUSBLuks() (*USBLuks, error) {
	return &USBLuks{}, nil
}

func (u *USBLuks) GetKey() ([]byte, error) {
	keyfile, err := ioutil.ReadFile("/sys/class/dmi/id/product_uuid")
	if err != nil {
		return []byte{}, err
	}

	return keyfile, nil
}

func (u *USBLuks) SetOption(key string, value string) error {
	switch key {
	case "devicepath":
		{
			u.options["device"] = value
		}
	case "keypath":
		{
			u.options["keypath"] = value
		}
	default:
		{
			return errors.Errorf("Invalid option: %v : %v", key, value)
		}
	}

	return nil
}

func (u *USBLuks) GetOption(key string) string {
	return u.options[key]
}
