// Copyright © 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package auth

import (
	"os/exec"
	"syscall"

	"github.com/pkg/errors"
	"golang.org/x/crypto/ssh/terminal"
)

type YubikeyLuks struct {
	options map[string]string
}

func NewYubikeyLuks() (*YubikeyLuks, error) {
	return &YubikeyLuks{}, nil
}

func (p *YubikeyLuks) GetKey() ([]byte, error) {
	key, err := terminal.ReadPassword(syscall.Stdin)

	if err != nil {
		return []byte{}, errors.WithMessage(err, "readPassword error\n")
	}

	challengeResponse := exec.Command("ykchalresp", "-2", "-i-")
	stdin, err := challengeResponse.StdinPipe()
	if err != nil {
		return []byte{}, errors.WithMessage(err, "failed to get stdin for Yubikey challenge-response\n")
	}

	go func() {
		defer stdin.Close()
		_, _ = stdin.Write(key)
	}()

	response, err := challengeResponse.CombinedOutput()
	if err != nil {
		return []byte{}, errors.WithMessage(err, "failed to receive response to challenge\n")
	}

	return response, nil
}

func (p *YubikeyLuks) SetOption(key string, value string) error {
	return errors.Errorf("Invalid option: %v", key)
}

func (p *YubikeyLuks) GetOption(key string) string {
	return p.options[key]
}
