// Copyright © 2019 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package gui

import (
	"log"
	"os/exec"

	"gitlab.com/redfield/installer/pkg/installer"
)

// PerformInstallWithGUI Creates an installation according to the information in InstallInfo, spawns GUI window
func PerformInstallWithGUI(i installer.InstallInfo, ui *UI) error {
	ui.UpdateProgress(0, "Creating partitions ...")
	err := installer.CreatePartitionTables(i.BlockDevice, i.PartitionTables)
	if err != nil {
		return err
	}

	err = installer.CreatePartitions(i.BlockDevice, i.Partitions)
	if err != nil {
		return err
	}

	ui.UpdateProgress(30, "Configuring LVM ...")
	err = installer.CreatePhysicalVolumes(i.PhysicalVolumes)
	if err != nil {
		return err
	}

	err = installer.CreateVolumeGroups(i.PhysicalVolumes)
	if err != nil {
		return err
	}

	err = installer.CreateLogicalVolumes(i.PhysicalVolumes)
	if err != nil {
		return err
	}

	ui.UpdateProgress(50, "Configuring disk encryption  ...")
	err = installer.CreateDiskCrypts(i.DiskCrypts)
	if err != nil {
		return err
	}

	err = installer.OpenDiskCrypts(i.DiskCrypts, false)
	if err != nil {
		return err
	}
	defer func() {
		err := installer.CloseDiskCrypts(i.DiskCrypts, false)
		if err != nil {
			log.Printf("%v", err)
		}
	}()

	ui.UpdateProgress(60, "Creating filesystems  ...")
	err = installer.CreateFileSystems(i.FileSystems, "install")
	if err != nil {
		return err
	}

	ui.UpdateProgress(70, "Configuring EFI  ...")
	err = installer.ConfigureEFI(i)
	if err != nil {
		return err
	}

	ui.UpdateProgress(80, "Installing files  ...")
	err = installer.PlaceFiles(i)
	if err != nil {
		return err
	}

	ui.UpdateProgress(100, "Installation Complete")
	return nil
}

// Reboot Reboot the system after installation finishes
func Reboot() {
	if err := exec.Command("/sbin/reboot", "-h", "now").Start(); err != nil {
		log.Printf("%v", err)
	}
}
