// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"gitlab.com/redfield/installer/pkg/auth"
)

// DiskCrypt holds info for doing disk encryption in the installer
type DiskCrypt struct {
	Path            string        `yaml:"path"`
	TargetName      string        `yaml:"target_name"`
	AuthMode        string        `yaml:"auth_mode"`
	AuthModeOptions []auth.Option `yaml:"auth_mode_options,omitempty,flow"`
}

// CreateDiskCrypt Create an encrypted disk volume.
func CreateDiskCrypt(dc DiskCrypt) error {
	a, err := auth.NewAuthenticator(dc.AuthMode, dc.AuthModeOptions)
	if err != nil {
		return err
	}

	err = LuksFormatVolume(dc.Path, a)
	if err != nil {
		return err
	}

	return nil
}

// CreateDiskCrypts Create a set of encrypted disk volumes.
func CreateDiskCrypts(dcs []DiskCrypt) error {
	for _, dc := range dcs {
		err := CreateDiskCrypt(dc)
		if err != nil {
			return err
		}
	}

	return nil
}

// OpenDiskCrypt Unlock an encrypted disk with a specified key
func OpenDiskCrypt(dc DiskCrypt) error {
	a, err := auth.NewAuthenticator(dc.AuthMode, dc.AuthModeOptions)
	if err != nil {
		return err
	}

	err = LuksOpenVolume(dc.Path, dc.TargetName, a)
	if err != nil {
		return err
	}

	return nil
}

// OpenDiskCrypts Unlock a set of encrypted disks
func OpenDiskCrypts(dcs []DiskCrypt, fightThrough bool) error {
	for _, dc := range dcs {
		err := OpenDiskCrypt(dc)
		if err != nil {
			if fightThrough {
				continue
			} else {
				return err
			}
		}
	}

	return nil
}

// CloseDiskCrypt Lock an encrypted disk
func CloseDiskCrypt(dc DiskCrypt) error {
	err := LuksCloseVolume(dc.TargetName)
	if err != nil {
		return err
	}

	return nil
}

// CloseDiskCrypts Lock a set of encrypted disks
func CloseDiskCrypts(dcs []DiskCrypt, ignoreFail bool) error {
	for _, dc := range dcs {
		err := CloseDiskCrypt(dc)
		if err != nil && !ignoreFail {
			return err
		}
	}

	return nil
}
