// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/pkg/errors"
)

// FileSystem Filesystem information
type FileSystem struct {
	Path      string `yaml:"path"`
	Type      string `yaml:"type"`
	Command   string `yaml:"command"`
	ExtraArgs string `yaml:"extra_args"`
	When      string `yaml:"when"`
}

func (f FileSystem) String() string {
	return fmt.Sprintf("Path: %v\nType: %v\nCommand: %v\nExtraArgs: %v", f.Path, f.Type, f.Command, f.ExtraArgs)
}

// Mount Mount information
type Mount struct {
	Source string `yaml:"source"`
	Point  string `yaml:"point"`
}

func (m Mount) String() string {
	return fmt.Sprintf("Source: %v\nPoint: %v", m.Source, m.Point)
}

// MountFileSystem Mounts a given source at a mount point
func MountFileSystem(m Mount) error {
	err := os.MkdirAll(m.Point, os.ModeDir)
	if err != nil {
		return err
	}

	mountFS, err := exec.Command("mount", m.Source, m.Point).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to mount filesystem: \n%s", mountFS)
	}

	return nil
}

// UnmountFileSystem Unmount a currently active mount
func UnmountFileSystem(m Mount) error {
	unmountFS, err := exec.Command("umount", m.Point).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to unmount filesystem: \n%s", unmountFS)
	}

	return nil
}

// CreateFileSystem Create a filesystem at a given path
func CreateFileSystem(fs FileSystem) error {
	createFS, err := exec.Command(fs.Command, fs.Path).CombinedOutput()

	if err != nil {
		return errors.Wrapf(err, "failed to create filesystem: \n%s", createFS)
	}

	return nil
}

// CreateFileSystems creates a filesystem for each FileSystem in fss whose
// When field matches action.
func CreateFileSystems(fss []FileSystem, action string) error {
	for _, fs := range fss {
		if fs.When != action {
			continue
		}

		err := CreateFileSystem(fs)
		if err != nil {
			return err
		}
	}

	return nil
}
