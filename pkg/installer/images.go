// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"

	"github.com/pkg/errors"
)

// InstallFile Describes a file for placement at a mounted destination
type InstallFile struct {
	SourceURI       string  `yaml:"source_uri"`
	DestinationFile string  `yaml:"destination_file"`
	Mounts          []Mount `yaml:"mount,flow"`
}

func (i InstallFile) String() string {
	return fmt.Sprintf("SourceURI: %v\nDestinationFile: %v\nMounts: %v", i.SourceURI, i.DestinationFile, i.Mounts)
}

// PlaceFile Mounts any necessary target volumes, obtains the source file
// using the provided credentials, places it at the required destination,
// and unmounts any mounted target volumes.
func PlaceFile(username string, password string, certificate string, i InstallFile, ignoreMountFailure bool) error {
	// Process required mounts for file placement
	for _, m := range i.Mounts {
		err := MountFileSystem(m)
		if err != nil && !ignoreMountFailure {
			return err
		}

		defer func(m Mount) {
			err := UnmountFileSystem(m)
			if err != nil {
				log.Printf("%v", err)
			}
		}(m)
	}

	uri, err := url.Parse(i.SourceURI)
	if err != nil {
		return nil
	}

	var response *http.Response

	switch uri.Scheme {
	case "file":
		{
			t := &http.Transport{}
			t.RegisterProtocol("file", http.NewFileTransport(http.Dir("/")))
			c := &http.Client{Transport: t}

			response, err = c.Get(uri.String())
			if err != nil {
				return err
			}

			defer response.Body.Close()
		}
	case "https", "http":
		{
			req, err := http.NewRequest(http.MethodGet, uri.String(), nil)
			if err != nil {
				return err
			}

			// With http, these credentials are sent in plaintext.
			if username != "" && password != "" {
				req.SetBasicAuth(username, password)
			}

			response, err = http.DefaultClient.Do(req)
			if err != nil {
				return err
			}

			defer response.Body.Close()

			if response.StatusCode != http.StatusOK {
				return errors.Errorf("failed to get %s: got %q", i.SourceURI, response.Status)
			}
		}
	default:
		return errors.Errorf("unsupported URI scheme: %s", uri.Scheme)
	}

	err = os.MkdirAll(path.Dir(i.DestinationFile), os.ModeDir)
	if err != nil {
		return err
	}

	f, err := os.Create(i.DestinationFile)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = io.Copy(f, response.Body)
	return err
}

// PlaceFiles Place a set of files
func PlaceFiles(i InstallInfo) error {
	for _, f := range i.InstallFiles {
		err := PlaceFile(i.Username, i.Password, i.Certificate, f, i.Upgrade)
		if err != nil {
			return err
		}
	}

	return nil
}
