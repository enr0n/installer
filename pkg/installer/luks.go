// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package installer

import (
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"github.com/pkg/errors"

	"gitlab.com/redfield/installer/pkg/auth"
)

func tempKeyFile(bytes []byte) (string, error) {
	tmpFile, err := ioutil.TempFile(os.TempDir(), "kf-")
	if err != nil {
		return "", err
	}

	path := tmpFile.Name()

	err = ioutil.WriteFile(path, bytes, 0600)
	if err != nil {
		return "", err
	}

	return path, nil
}

func scrubKeyFile(path string) {
	// always attempt removal
	defer os.Remove(path)

	file, err := os.OpenFile(path, os.O_RDWR, 0666)
	if err != nil {
		log.Printf("Failed to scrub keyfile (%v) - openfile failed with: %v\n", path, err)
		return
	}
	defer file.Close()

	fileInfo, err := file.Stat()
	if err != nil {
		log.Printf("Failed to scrub keyfile (%v) - stat failed with: %v\n", path, err)
		return
	}

	size := fileInfo.Size()

	junk := make([]byte, size)

	n, err := file.Write(junk)
	if err != nil {
		log.Printf("Failed to scrub keyfile (%v) - write failed with: %v\n", path, err)
	}

	if int64(n) < size {
		log.Printf("Short changed on scrubbing keyfile - overwrote %v of %v bytes\n", n, size)
	}
}

func LuksAddKey(luksVolumePath string, currentAuth auth.Authenticator, addAuth auth.Authenticator) error {
	currentKey, err := currentAuth.GetKey()
	if err != nil {
		return err
	}

	newKey, err := addAuth.GetKey()
	if err != nil {
		return err
	}

	addKeyFile, err := tempKeyFile(newKey)
	if err != nil {
		return err
	}
	defer scrubKeyFile(addKeyFile)

	luksAdd := exec.Command("cryptsetup", "luksAddKey", luksVolumePath, addKeyFile, "-d", "-")

	stdin, err := luksAdd.StdinPipe()
	if err != nil {
		return errors.New("failed to get stdin for adding luks key")
	}

	go func() {
		defer stdin.Close()

		_, _ = stdin.Write(currentKey)
	}()

	stdErrStdOut, err := luksAdd.CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to run luksAdd: %s", stdErrStdOut)
	}

	return nil
}

func LuksChangeKey(luksVolumePath string, oldAuth auth.Authenticator, newAuth auth.Authenticator) error {
	oldKey, err := oldAuth.GetKey()
	if err != nil {
		return err
	}

	newKey, err := newAuth.GetKey()
	if err != nil {
		return err
	}

	out, err := exec.Command("cryptsetup", "luksChangeKey", luksVolumePath, string(newKey), "--key-file", string(oldKey)).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to exec cryptsetup luksChangeKey: %s", out)
	}

	return nil
}

func LuksRemoveKey(luksVolumePath string, auth auth.Authenticator) error {
	key, err := auth.GetKey()
	if err != nil {
		return err
	}

	out, err := exec.Command("cryptsetup", "luksRemoveKey", luksVolumePath, string(key)).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to exec cryptsetup luksRemoveKey: %s", out)
	}

	return nil
}

func LuksOpenVolume(luksVolumePath string, luksVolumeName string, auth auth.Authenticator) error {
	luksOpen := exec.Command("cryptsetup", "luksOpen", luksVolumePath, "-d", "-", luksVolumeName)

	stdin, err := luksOpen.StdinPipe()
	if err != nil {
		return errors.New("failed to get stdin for partition decryption")
	}

	go func() {
		defer stdin.Close()
		key, err := auth.GetKey()
		if err != nil {
			log.Printf("Failed to get auth key while opening luks volume: %v", err)
		}

		_, _ = stdin.Write(key)
	}()

	stdErrStdOut, err := luksOpen.CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to open encrypted storage volume %s", stdErrStdOut)
	}

	return nil
}

func LuksCloseVolume(luksVolumeName string) error {
	luksClose, err := exec.Command("cryptsetup", "luksClose", luksVolumeName).CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to close encrypted volume %s", luksClose)
	}

	return nil
}

func LuksFormatVolume(targetPath string, auth auth.Authenticator) error {
	luksFormat := exec.Command("cryptsetup", "-q", "luksFormat", targetPath, "-d", "-")
	stdin, err := luksFormat.StdinPipe()
	if err != nil {
		return errors.New("failed to get stdin for partition encryption")
	}

	go func() {
		defer stdin.Close()
		key, err := auth.GetKey()
		if err != nil {
			log.Printf("failed to get auth key while formatting luks volume: %v", err)
		}

		_, _ = stdin.Write(key)
	}()

	stdErrStdOut, err := luksFormat.CombinedOutput()
	if err != nil {
		return errors.Wrapf(err, "failed to run luksFormat on targetPath: %v\n%s", targetPath, stdErrStdOut)
	}

	return nil
}
